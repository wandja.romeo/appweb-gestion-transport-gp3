package com.example.appwebgestiontransportgp3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppwebGestionTransportGp3Application {

	public static void main(String[] args) {
		SpringApplication.run(AppwebGestionTransportGp3Application.class, args);
	}

}
